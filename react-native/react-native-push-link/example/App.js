import React, {Component} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {PUSH_LINK_API_KEY} from '@env';
import {TouchableOpacity, StyleSheet, Image, Text, View} from 'react-native';
import PushLink from 'react-native-push-link';

export default class App extends Component {
  pushLinkStart = async () => {
    const deviceId = await PushLink.getDeviceId().catch((e) => e);

    const pushLinkStarted = await PushLink.start(
      PUSH_LINK_API_KEY,
      deviceId,
    ).catch((e) => e);

    console.log('pushLinkStarted', pushLinkStarted);

    const strategyCustom = await PushLink.setStrategyCustom(
      {TypeBroadcastReceiver: 'APPLY'},
      (responseBroadcast) => {
        console.log(responseBroadcast);
      },
    ).catch((e) => e);

    console.log(strategyCustom);

    const strategy = await PushLink.getCurrentStrategy().catch((e) => e);

    console.log(strategy);
  };

  installApk = async () => {
    const install = await PushLink.installApk().catch((e) => e);
    console.log(install);
  };

  componentDidMount = () => {
    PushLink.captureGlobalErrorsReactNative(true);
    SplashScreen.hide();
  };

  generateError = () => {
    //PushLink.captureErrorHandlerReactNative();
    const a = '';
    console.log(a.teste.a);
  };

  render() {
    return (
      <>
        <View style={styles.container}>
          <Image
            style={styles.tinyLogo}
            source={require('./assets/push-link.png')}
          />

          <Text style={styles.welcome}>Teste Update 2</Text>

          <TouchableOpacity style={styles.button} onPress={this.pushLinkStart}>
            <Text>Start PushLink</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={this.installApk}>
            <Text>Install APK</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button} onPress={this.generateError}>
            <Text>Gerar ERROR</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    color: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white',
    marginBottom: 20,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    marginTop: 20,
  },
  tinyLogo: {
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'contain',
    width: '80%',
    height: 150,
  },
});
